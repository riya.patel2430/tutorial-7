const express = require("express");
const { v4: uuidv4 } = require("uuid");
const { MongoClient, ObjectID } = require("mongodb");

const app = express();
app.use(express.json());

// MongoDB Atlas connection URI
const uri =
  "mongodb+srv://riyapatel2430:riyapatel@csci5708.qpc6g7x.mongodb.net/";

// Database name and collection name
const dbName = "users";
const collectionName = "users";

// Function to connect to the MongoDB Atlas cluster
async function connectToDatabase() {
  try {
    const client = await MongoClient.connect(uri, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("Connected to MongoDB Atlas");
    return client.db(dbName).collection(collectionName);
  } catch (error) {
    console.error("Failed to connect to the database:", error);
    process.exit(1);
  }
}

// GET request to retrieve all users
app.get("/users", async (req, res) => {
  const collection = await connectToDatabase();
  try {
    const users = await collection.find().toArray();
    res.json({
      message: "Users retrieved",
      success: true,
      users: users,
    });
  } catch (error) {
    console.error("Failed to fetch users:", error);
    res.status(500).json({ success: false, message: "Failed to fetch users" });
  }
});

// PUT request to update user details
app.put("/update/:id", async (req, res) => {
  const id = req.params.id;
  const { email, firstName } = req.body;

  const collection = await connectToDatabase();
  try {
    const result = await collection.updateOne(
      { _id: ObjectID(id) },
      { $set: { email: email, firstName: firstName } }
    );

    if (result.matchedCount === 0) {
      res.status(404).json({ success: false, message: "User not found" });
    } else {
      res.json({ success: true, message: "User updated" });
    }
  } catch (error) {
    console.error("Failed to update user:", error);
    res.status(500).json({ success: false, message: "Failed to update user" });
  }
});

// POST request to add a new user
app.post("/add", async (req, res) => {
  const { email, firstName } = req.body;

  const collection = await connectToDatabase();
  try {
    const newUser = {
      email: email,
      firstName: firstName,
    };

    const result = await collection.insertOne(newUser);

    console.log("User added:", newUser);
    res.json({ success: true, message: "User added" });
  } catch (error) {
    console.error("Failed to add user:", error);
    res.status(500).json({ success: false, message: "Failed to add user" });
  }
});

// GET request to retrieve a single user by ID
app.get("/user/:id", async (req, res) => {
  const id = req.params.id;

  const collection = await connectToDatabase();
  try {
    const user = await collection.findOne({ _id: ObjectID(id) });

    if (!user) {
      res.status(404).json({ success: false, message: "User not found" });
    } else {
      res.json({ success: true, user: user });
    }
  } catch (error) {
    console.error("Failed to fetch user:", error);
    res.status(500).json({ success: false, message: "Failed to fetch user" });
  }
});

// DELETE request to delete a user by ID
app.delete("/delete/:id", async (req, res) => {
  const id = req.params.id;

  const collection = await connectToDatabase();
  try {
    const result = await collection.deleteOne({ _id: ObjectID(id) });

    if (result.deletedCount === 0) {
      res.status(404).json({ success: false, message: "User not found" });
    } else {
      res.json({ success: true, message: "User deleted" });
    }
  } catch (error) {
    console.error("Failed to delete user:", error);
    res.status(500).json({ success: false, message: "Failed to delete user" });
  }
});

// Handle invalid routes
app.use((req, res) => {
  res.status(404).json({ success: false, message: "Route not found" });
});

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
